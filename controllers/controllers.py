# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request

# Page for external users


class FurnitureExternal(http.Controller):
    @http.route('/furniture/external/', auth='public')
    def index(self, **kw):
        values = {
            'message': 'Welcome to our Company',
            'list_title': 'Our products',
            'products': ['beds', 'tables', 'chairs']
        }
        return request.render("furniture.external_page", values)


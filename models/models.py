# -*- coding: utf-8 -*-

from odoo import models, fields, api


class FurnitureProductTemplate(models.Model):
    _name = 'product.template'
    _inherit = 'product.template'

    height = fields.Float('Height')
    length = fields.Float('Length')
    width = fields.Float('Width')
    delivery_cost = fields.Float('Delivery Cost')
